import os
import sys
import tempfile
from pathlib import Path

import pytest

import pop.hub


@pytest.fixture
def tempdir_hub():
    with tempfile.TemporaryDirectory() as tempdir:
        try:
            pwd = os.getcwd()
            os.chdir(tempdir)
            hub = pop.hub.Hub()
            yield tempdir, hub
        finally:
            os.chdir(pwd)


@pytest.fixture
def hub_with_contracted_sub(tempdir_hub):
    tempdir, hub = tempdir_hub
    p = Path(tempdir) / "test_rec_contracts" / "test_sub"
    try:
        p.mkdir(parents=True, exist_ok=True)
        sys.path.insert(0, str(p.parent.parent))
        conf = p.parent / "conf.py"
        conf.write_text('DYNE={"test_sub": ["test_sub"]}')
        scripts = p / "scripts.py"
        version = p / "version.py"
        thing = p / "thing.py"
        thing.write_text("def fun(hub): return 'so much fun'")
        scripts.write_text("True")
        version.write_text("version=1")

        recursive_contracts = p / "recursive_contracts"
        recursive_contracts.mkdir()
        all_recursive_contracts = recursive_contracts / "init.py"
        all_recursive_contracts.write_text(
            """
def pre(hub, ctx):
    print("recursive-pre")
    ctx.extra = (getattr(ctx, 'extra', None) or []) + ["recursive-pre"]

def post(hub, ctx):
    print("recursive-post")
    ctx.ret.append('recursive-post')

def call(hub, ctx):
    print('recursive-pre-call')
    result = ctx.extra + ['recursive-pre-call', ctx.func(hub=hub), 'recursive-post-call']
    print('recursive-post-call')
    return result
"""
        )

        nested_sub = p / "nested_sub"
        nested_sub.mkdir(exist_ok=True, parents=True)
        init_contracts = nested_sub / "contracts" / "init.py"
        init_contracts.parent.mkdir()
        init_contracts.write_text(
            """
def pre(hub, ctx):
    print('regular-pre')
    ctx.extra = (getattr(ctx, 'extra', None) or []) + ["regular-pre"]

def post(hub, ctx):
    print('regular-post')
    ctx.ret.append('regular-post')

def call(hub, ctx):
    print('regular-pre-call')
    result = ctx.extra + ['regular-pre-call', ctx.func(hub=hub), 'regular-post-call']
    print('regular-post-call')
    return result
"""
        )
        fnord = nested_sub / "fnord.py"
        fnord.write_text(
            """
def test_call(hub):
    print("actual-call")
    return 'actual-call'
"""
        )
        hub.pop.sub.add(dyne_name="test_sub")
        yield hub
    finally:
        try:
            sys.path.remove(str(p.parent.parent))
        except ValueError:
            pass


def test_when_dyne_sub_is_added_with_recursive_contracts_the_contracts_should_be_applied(
    capsys, hub_with_contracted_sub
):
    hub = hub_with_contracted_sub
    expected_result = [
        "regular-pre",
        "recursive-pre",
        "regular-pre-call",
        "actual-call",
        "regular-post-call",
        "regular-post",
        "recursive-post",
    ]
    expected_output = "regular-pre\nrecursive-pre\nregular-pre-call\nactual-call\nregular-post-call\nregular-post\nrecursive-post\n"
    hub.pop.sub.load_subdirs(hub.test_sub, recurse=True)

    actual_result = hub.test_sub.nested_sub.fnord.test_call()

    output = capsys.readouterr()
    assert output.out == expected_output
    assert actual_result == expected_result
